![Build Status](https://gitlab.com/EAL-ITT/21a-itt1-embedded/badges/master/pipeline.svg)


# 21A-ITT1-EMBEDDED

weekly plans, resources and other relevant stuff for courses.

public website for students:

*  [gitlab pages](https://EAL-ITT.gitlab.io/21a-itt1-embedded/)


## Usage

Directories and usage

* `docs`: Contains docs to be published to pages. `.md` files will be converted to pdf, as well as html, and `.pdf` files will kept as-is.   
This directory must contain at least one .md document besides readme.md
* `weekly_plans`: Files of called `weekly_ww??.md` will be concatenated and put into `docs`. They will also be used in html in pages.
* `exercises`: As `weekly_plans`, but intended for exercises.
* `site`: The jekyl files that will be used to generate the static ste to be put on pages. This includes the weekly html and also the generated pdf from `docs`.

    The files are templates that may be edited as any other jekyll sites to add extra stuff if relevant.

* `scripts`: scripts needed to make the automation on the site.
* `.gitlab-ci`: the "script" used by gitlab when files are changed and pushed to gitlab.com.

* `project-info`: Used to initiatlize the repo with appropriate names in the various locations.
* `init_project.sh`: Script that does the actual text replacement in files. Idem potent, and may be run multiple times, but replacement only happens the first time.

The course template installation guide:

* [https://eal-itt.gitlab.io/new-course-guide/index.html](https://eal-itt.gitlab.io/new-course-guide/index.html)
