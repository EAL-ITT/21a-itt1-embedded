---
title: '21A ITT1 Embedded Systems'
filename: '21A_ITT1_EMBEDDED_OLA15'
subtitle: 'OLA15'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-10-07
email: 'nisi@ucl.dk'
left-header: \today
right-header: OLA15
skip-toc: true
---

# Introduction

The compulsory learning activity **OLA15** is an exercise where you show what you have learned so far in embedded systems.  
Note that this is an individual hand-in.  

Make sure to read and understand both the report and exercise requirements before starting to work.  

An overview of obligatory learning activities on 1st semester, including dates and terms, can be found in the [1st semester description 2021](https://esdhweb.ucl.dk/D21-1625731.pdf?nocache=81d2c836-a104-4c84-b4bc-596c7cb92ba6&_gl=1*1kw82dz*_up*MQ..*_ga*MTg5NTY4Njk1OS4xNjMzNTkzMjI1*_ga_1R6WX2EYMV*MTYzMzU5MzIyNS4xLjEuMTYzMzU5MzIyNS4w&_ga=2.108443010.1290386729.1633593226-1895686959.1633593225)  
Report handin as well as deadline for submission is done through wiseflow.   

# Report content requirements

The hand-in is a small report in pdf format. The report needs to be handed in on wiseflow before the deadline. 
I expect 3-5 standard pages.  
A standard page means 2,400/x characters including spaces and footnotes.  
Images are not included in the character count.  
Make sure to format the report beautiful and readable.  
Make sure to correct spelling and punctuation.  

The report needs to contain the following:

* Frontpage with name, semester, course, school logo, date and character count
* Numbered table of contents including page numbers for each chapter

* A Presentation of your solution to the exercise specified below including: 
    1. A copy of your code in the report + link to your code on gitlab
    2. An explanation of each line in the code
    3. An image of your system (PICO and connected components)
    4. A description of the actions you are doing to confirm that the system is working as intended (troubleshooting while developing)
    5. A description of how the functionality of the system could be improved from a user point of view (how responsive is the system when pushing the buttons? etc.)
    6. An outlook of how the system can be expanded (your own thougths)
    7. Link to your code on gitlab
    
# Exercise requirements

### Information

This is the exercise that you have to write a report about.  
The exercise is a small embedded systems built around the RPi Pico.   
The system peripherals are 2 external led's, 2 buttons and the RPi Pico onboard LED.  

### Exercise instructions

1. Connect your hardware according to this image:  

    ![pico 2 buttons and 2 led's](pico_2btns_2leds.png)
2. create a new python file
3. Initialize your inputs and outputs (init HW) as shown below:  

    ```py
    led1 = machine.Pin(14, machine.Pin.OUT) 
    led2 = machine.Pin(15, machine.Pin.OUT) 
    btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
    btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
    led_onboard = machine.Pin(25, machine.Pin.OUT)
    ```
4. Implement the below flowchart in your python file    

    ![program2](ww40_program2.png)
5. Use functions for all proces symbols in the flowchart (square boxes)
6. Test the program and confirm fuctionality
7. Correct the program if you experience any unexpected behaviour
8. Use your gitlab project to backup the program


Follow-up
============

After hand-in, the lecturer will review the documents. Results are communicated through wiseflow.

