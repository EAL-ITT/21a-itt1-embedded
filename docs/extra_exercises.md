---
title: '21A ITT1 EMBEDDED'
subtitle: 'Extra exercises'
filename: 'extra_exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Extra exercises
skip-toc: false
semester: 21A
---

## Exercise 1 - 1 external LED and Button

### Information

This exercise is for practice using external led's and buttons. It is also a warm up to later exercises about connecting 2 raspberry Pico's.

### Exercise instructions

1. Connect your hardware according to this image: 
    ![pico 2 buttons and 2 led's](pico_1btn_1led.png)
2. create a new python file
3. Initialize your inputs and outputs as shown below:
    ```Python
    led1 = machine.Pin(14, machine.Pin.OUT) 
    btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
    led_onboard = machine.Pin(25, machine.Pin.OUT)
    ```
4. Implement the below flowchart in your python file  
    ![program1](ww40_program1.png)
5. Test the program and confirm fuctionality
6. Correct the program if you experience any unexpected behaviour
7. Use your gitlab project to backup the program

### Links

If needed put links here

## Exercise 2 - 2 external LED's and Buttons - program 1

### Information

This exercise is for practice using external led's and buttons. It is also a warm up to later exercises about connecting 2 raspberry Pico's.

### Exercise instructions

1. Connect your hardware according to this image: 
    ![pico 2 buttons and 2 led's](pico_2btns_2leds.png)
2. create a new python file
3. Initialize your inputs and outputs as shown below:
    ```Python
    led1 = machine.Pin(14, machine.Pin.OUT) 
    led2 = machine.Pin(15, machine.Pin.OUT) 
    btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
    btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
    led_onboard = machine.Pin(25, machine.Pin.OUT)
    ```
4. Implement the below flowchart in your python file    
    ![program2](ww40_program2.png)
5. Test the program and confirm fuctionality
6. Correct the program if you experience any unexpected behaviour
7. Use your gitlab project to backup the program

## Exercise 3 - 2 external LED's and Buttons - program 2

### Information

This exercise is for practice using external led's and buttons. It is also a warm up to later exercises about connecting 2 raspberry Pico's.

### Exercise instructions

1. Connect your hardware according to this image: 
    ![pico 2 buttons and 2 led's](pico_2btns_2leds.png)
2. create a new python file
3. Initialize your inputs and outputs as shown below:
    ```Python
    led1 = machine.Pin(14, machine.Pin.OUT) 
    led2 = machine.Pin(15, machine.Pin.OUT) 
    btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
    btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
    led_onboard = machine.Pin(25, machine.Pin.OUT)
    ```
4. Implement the below flowchart in your python file  
    ![program3](ww40_program3.png)
5. Test the program and confirm fuctionality
6. Correct the program if you experience any unexpected behaviour
7. Use your gitlab project to backup the program

## Exercise 4 - 2 external LED's and Buttons - program 3

### Information

This exercise is for practice using external led's and buttons. It is also a warm up to later exercises about connecting 2 raspberry Pico's.

### Exercise instructions

1. Connect your hardware according to this image: 
    ![pico 2 buttons and 2 led's](pico_2btns_2leds.png)
2. create a new python file
3. Initialize your inputs and outputs as shown below:
    ```Python
    led1 = machine.Pin(14, machine.Pin.OUT) 
    led2 = machine.Pin(15, machine.Pin.OUT) 
    btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
    btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
    led_onboard = machine.Pin(25, machine.Pin.OUT)
    ```
4. Implement the below flowchart in your python file  
    ![program4](ww40_program4.png)
5. Test the program and confirm fuctionality
6. Correct the program if you experience any unexpected behaviour
7. Use your gitlab project to backup the program

## Exercise 4 - loops and code indentation

### Information

Overview information

### Exercise instructions

1. Follow the book chapter 2 instructions in the section named "Next steps: loops and code indentation"
2. CHALLENGE: LOOP THE LOOP
Can you change the loop back into a definite loop again? Can you
add a second definite loop to the program? How would you add a
loop within a loop, and how would you expect that to work?
3. Backup your code to your Gitlab embedded project

### Links

If needed put links here