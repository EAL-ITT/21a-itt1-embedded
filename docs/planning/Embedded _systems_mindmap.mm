<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Embedded Systems" FOLDED="false" ID="ID_107805750" CREATED="1610381326375" MODIFIED="1621335402317"><hook NAME="MapStyle" zoom="1.952">
    <properties show_icon_for_attributes="true" .addon.mm="file" fit_to_viewport="false" show_note_icons="true" edgeColorConfiguration="#808080ff,#000000ff,#ff0033ff,#009933ff,#3333ffff,#ff6600ff,#cc00ccff,#ffbf00ff,#00ff99ff,#0099ffff,#996600ff,#000000ff,#cc0066ff,#33ff00ff,#ff9999ff,#0000ccff,#cccc00ff,#0099ccff,#006600ff,#ff00ccff,#00cc00ff,#0066ccff,#00ffffff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt" TEXT_SHORTENED="true">
<font SIZE="24"/>
<richcontent CONTENT-TYPE="plain/" TYPE="DETAILS"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<font SIZE="9"/>
<stylenode LOCALIZED_TEXT="default" ID="ID_1273250224" ICON_SIZE="12 pt" COLOR="#000000" STYLE="bubble" SHAPE_VERTICAL_MARGIN="0 pt" TEXT_ALIGN="CENTER" MAX_WIDTH="120 pt" MIN_WIDTH="120 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1273250224" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="Arial" SIZE="9" BOLD="true" ITALIC="false"/>
<edge STYLE="bezier" WIDTH="3"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" TEXT_ALIGN="LEFT">
<font SIZE="11" BOLD="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes" COLOR="#000000" BACKGROUND_COLOR="#ffffff">
<font SIZE="9" BOLD="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT">
<font BOLD="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<font SIZE="9"/>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_1358928635">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#0000ff" TRANSPARENCY="255" DESTINATION="ID_1358928635"/>
<edge COLOR="#0000cc"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<font SIZE="9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" UNIFORM_SHAPE="true" MAX_WIDTH="120 pt" MIN_WIDTH="120 pt">
<font SIZE="24" ITALIC="true"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font SIZE="18"/>
<hook NAME="AutomaticEdgeColor" COUNTER="0" RULE="FOR_COLUMNS"/>
<node TEXT="Knowledge" POSITION="right" ID="ID_1590053677" CREATED="1621327178949" MODIFIED="1621327382935" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Communication" ID="ID_1481415324" CREATED="1621327247586" MODIFIED="1638959142021" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">communications and interface equipment in general, and how it is applied in selected solutions;</font>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Parallel" ID="ID_557264652" CREATED="1621412871308" MODIFIED="1621412918337"/>
<node TEXT="Serial" ID="ID_1859691340" CREATED="1621412918810" MODIFIED="1621412922638"/>
<node TEXT="Error checking" ID="ID_798188003" CREATED="1621412925061" MODIFIED="1621412937056"/>
<node TEXT="Encryption" ID="ID_595514533" CREATED="1621412940677" MODIFIED="1621412945946"/>
</node>
<node TEXT="Interface" ID="ID_768716487" CREATED="1621328430699" MODIFIED="1621410775328" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">communications and interface equipment in general, and how it is applied in selected solutions;</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Busses" ID="ID_1160881924" CREATED="1621412958743" MODIFIED="1621412969144"/>
<node TEXT="Connectors" ID="ID_936229493" CREATED="1621412973683" MODIFIED="1621412980164"/>
<node TEXT="Logic levels" ID="ID_434864744" CREATED="1621412983830" MODIFIED="1621412987797"/>
<node TEXT="Timing diagram" ID="ID_563813170" CREATED="1621412992605" MODIFIED="1621412999970"/>
<node TEXT="API" ID="ID_1662539113" CREATED="1621423014810" MODIFIED="1621423018387"/>
</node>
<node TEXT="Modules" ID="ID_1568220166" CREATED="1621327320132" MODIFIED="1621410806547" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">electronic modules in overview, and how selected modules are constructed</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Actuators" ID="ID_1314915428" CREATED="1621422931016" MODIFIED="1621422938618"/>
<node TEXT="Sensors" ID="ID_461842773" CREATED="1621425095177" MODIFIED="1621425097413"/>
<node TEXT="MEMS" ID="ID_1036891742" CREATED="1621425105526" MODIFIED="1621425107851"/>
<node TEXT="Analog" ID="ID_1002849557" CREATED="1621425116372" MODIFIED="1621425124952"/>
<node TEXT="Digital" ID="ID_1963791813" CREATED="1621425126234" MODIFIED="1621425129452"/>
</node>
<node TEXT="Protocols" ID="ID_346651901" CREATED="1621327485052" MODIFIED="1621410776872" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">protocols, including communication protocols, their structure and what the differences and application options are</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="I2C" ID="ID_388916715" CREATED="1621422960311" MODIFIED="1621422982007"/>
<node TEXT="SPI" ID="ID_1835752632" CREATED="1621422983871" MODIFIED="1621422986371"/>
<node TEXT="UART" ID="ID_1386480116" CREATED="1621422986997" MODIFIED="1621422990352"/>
<node TEXT="MQTT" ID="ID_1066630762" CREATED="1621422997232" MODIFIED="1621423000141"/>
<node TEXT="COAP" ID="ID_1899151632" CREATED="1621423000902" MODIFIED="1621423003470"/>
</node>
<node TEXT="IoT" ID="ID_1525185713" CREATED="1621327541841" MODIFIED="1638956396304" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">Internet of Things technologies, their structure in general and selected solutions in more detail</font>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Power consumption" ID="ID_397163525" CREATED="1621423033461" MODIFIED="1621423042821"/>
<node TEXT="Infrastructure" ID="ID_1654882230" CREATED="1621423045156" MODIFIED="1621423055152"/>
<node TEXT="Publish/Subscribe" ID="ID_1987234454" CREATED="1621423060365" MODIFIED="1621423066920"/>
<node TEXT="IIoT" ID="ID_440794411" CREATED="1621423069141" MODIFIED="1621423072477"/>
<node TEXT="Smart city" ID="ID_1562253210" CREATED="1621423072902" MODIFIED="1621423080911"/>
</node>
<node TEXT="Technical math" ID="ID_1849022227" CREATED="1621327574123" MODIFIED="1621423032237" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">technical mathematics applied to the subject area as needed to understand electronics and/or communications</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Signal processing" ID="ID_724271290" CREATED="1621423088828" MODIFIED="1621423093467"/>
<node TEXT="Feedback and control" ID="ID_213585796" CREATED="1621423100797" MODIFIED="1621423106968"/>
<node TEXT="Electronics" ID="ID_771869295" CREATED="1621423110406" MODIFIED="1621423115926"/>
</node>
<node TEXT="OS" ID="ID_189773390" CREATED="1621327596310" MODIFIED="1621410809871" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">operating systems,their characteristics and uses</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Linux" ID="ID_1376408496" CREATED="1621423123842" MODIFIED="1621423126724"/>
<node TEXT="RT Thread (RTOS)" ID="ID_945813859" CREATED="1621423127892" MODIFIED="1621423139211"/>
<node TEXT="FreeRTOS" ID="ID_1378984804" CREATED="1621423140018" MODIFIED="1621423143959"/>
<node TEXT="State machines" ID="ID_605830302" CREATED="1621427722387" MODIFIED="1621427726889"/>
<node TEXT="Kernel" ID="ID_1768034519" CREATED="1621427730032" MODIFIED="1621427732379"/>
<node TEXT="Time slicing" ID="ID_1632776558" CREATED="1621427733028" MODIFIED="1621427738791"/>
<node TEXT="Busy wait" ID="ID_805168583" CREATED="1621427742255" MODIFIED="1621427749613"/>
</node>
<node TEXT="Signal handling" ID="ID_698790569" CREATED="1621327617669" MODIFIED="1621410811324" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">signal handling in a general sense, and how it is used and incorporated in solutions</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Signal conditioning" ID="ID_727933522" CREATED="1621423149493" MODIFIED="1621423154557"/>
<node TEXT="Sampling" ID="ID_1368909223" CREATED="1621423160983" MODIFIED="1621423172169"/>
<node TEXT="Noise" ID="ID_1502307673" CREATED="1621423173723" MODIFIED="1621423176086"/>
<node TEXT="Logic levels" ID="ID_1270194406" CREATED="1621423182201" MODIFIED="1621423186235"/>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_867671575" CREATED="1621327295735" MODIFIED="1621327295735">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1220681286" CREATED="1621327304415" MODIFIED="1621327304415">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Skills" POSITION="right" ID="ID_542656331" CREATED="1621327662007" MODIFIED="1621327675988" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="select" ID="ID_1251924343" CREATED="1621327744167" MODIFIED="1621327915251" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Datasheets" ID="ID_1322499796" CREATED="1621423201594" MODIFIED="1621423205406"/>
<node TEXT="Requirements" ID="ID_1109424252" CREATED="1621423205965" MODIFIED="1621423209995"/>
<node TEXT="Processor types" ID="ID_1982176202" CREATED="1621424680738" MODIFIED="1621424686342"/>
</node>
<node TEXT="adapt" ID="ID_617854011" CREATED="1621327758449" MODIFIED="1621327915251" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Sensors" ID="ID_1617234897" CREATED="1621427862786" MODIFIED="1621427864790"/>
<node TEXT="Signals" ID="ID_1575738123" CREATED="1621427865260" MODIFIED="1621427868707"/>
<node TEXT="uC configuration" ID="ID_749796269" CREATED="1621427887643" MODIFIED="1621427899734"/>
</node>
<node TEXT="apply" ID="ID_59423744" CREATED="1621327767520" MODIFIED="1621327915251" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Implement" ID="ID_255252547" CREATED="1621423270832" MODIFIED="1621423279380"/>
</node>
<node TEXT="secure" ID="ID_492415575" CREATED="1621327778313" MODIFIED="1621327915251" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Physical access" ID="ID_628840439" CREATED="1621423285318" MODIFIED="1621423301413"/>
<node TEXT="Remote access" ID="ID_687559583" CREATED="1621423289596" MODIFIED="1621423296845"/>
<node TEXT="IoT security standards" ID="ID_341504691" CREATED="1621423304446" MODIFIED="1621423312484"/>
<node TEXT="User safety" ID="ID_280403170" CREATED="1621423341506" MODIFIED="1621423345938"/>
<node TEXT="Machine directive" ID="ID_661675823" CREATED="1621423346835" MODIFIED="1621423353590"/>
<node TEXT="CE etc." ID="ID_170287785" CREATED="1621423354419" MODIFIED="1621423378027"/>
<node TEXT="Recovery" ID="ID_1147848826" CREATED="1621423423493" MODIFIED="1621423426533"/>
<node TEXT="Redundancy" ID="ID_1617462102" CREATED="1621424653290" MODIFIED="1621424665103"/>
</node>
<node TEXT="sustainable" ID="ID_489242472" CREATED="1621327790332" MODIFIED="1621327915251" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Power consumption" ID="ID_95596374" CREATED="1621423315788" MODIFIED="1621423321188"/>
<node TEXT="Product life cycle" ID="ID_1923358264" CREATED="1621423324808" MODIFIED="1621423330614"/>
</node>
<node TEXT="test systems" ID="ID_1006319321" CREATED="1621327806618" MODIFIED="1621423257263" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">construct and use test systems</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="HW testing" ID="ID_1710589838" CREATED="1621423386344" MODIFIED="1621423389206"/>
<node TEXT="SW testing" ID="ID_160697309" CREATED="1621423389428" MODIFIED="1621423392851"/>
<node TEXT="Unit testing" ID="ID_580469756" CREATED="1621423393321" MODIFIED="1621423396811"/>
<node TEXT="System testing" ID="ID_910026412" CREATED="1621423398270" MODIFIED="1621423403359"/>
<node TEXT="Test plan" ID="ID_180429456" CREATED="1621423410574" MODIFIED="1621423412921"/>
<node TEXT="Modularization" ID="ID_1483989066" CREATED="1621423413820" MODIFIED="1621423419512"/>
<node TEXT="Test rig" ID="ID_751769991" CREATED="1621427993179" MODIFIED="1621427995311"/>
<node TEXT="Emulation" ID="ID_886007470" CREATED="1621427997712" MODIFIED="1621428001634"/>
<node TEXT="Automated tests" ID="ID_1416403035" CREATED="1621428005070" MODIFIED="1621428011446"/>
</node>
<node TEXT="Document" ID="ID_1690717596" CREATED="1621327846924" MODIFIED="1621423382767" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">document and communicate tasks and solutions using embedded components and systems</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Block diagrams" ID="ID_897445197" CREATED="1621423432242" MODIFIED="1621423436477"/>
<node TEXT="Flow charts" ID="ID_1241683921" CREATED="1621423437623" MODIFIED="1621423441314"/>
<node TEXT="User manual" ID="ID_953062103" CREATED="1621423442506" MODIFIED="1621423449124"/>
<node TEXT="Test plan" ID="ID_693382565" CREATED="1621423450650" MODIFIED="1621423458770"/>
<node TEXT="Development plan" ID="ID_1614335651" CREATED="1621423460193" MODIFIED="1621423463542"/>
</node>
<node TEXT="Communicate" ID="ID_1688430724" CREATED="1621327851259" MODIFIED="1621410841821" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">document and communicate tasks and solutions using embedded components and systems</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Customer requirements" ID="ID_1490304832" CREATED="1621423485792" MODIFIED="1621423491737"/>
<node TEXT="Domain knowledge" ID="ID_1739477496" CREATED="1621423493490" MODIFIED="1621423501794"/>
<node TEXT="Questionarie" ID="ID_1850538599" CREATED="1621423508740" MODIFIED="1621423514799"/>
</node>
</node>
<node TEXT="Competencies" POSITION="right" ID="ID_1453374657" CREATED="1621327887801" MODIFIED="1621327915251" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="analysis" ID="ID_1535613084" CREATED="1621328210546" MODIFIED="1621410779102" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">undertake analysis, needs analysis, design, development and testing of secure embedded and sustainable solutions</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Use case" ID="ID_1054853783" CREATED="1621424967722" MODIFIED="1621424971233"/>
</node>
<node TEXT="needs analysis" ID="ID_342109524" CREATED="1621328250178" MODIFIED="1621328413661" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Customer needs vs real needs" ID="ID_1468893280" CREATED="1621424976109" MODIFIED="1621424983473"/>
</node>
<node TEXT="design" ID="ID_720792538" CREATED="1621328275199" MODIFIED="1638959212871" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">undertake analysis, needs analysis, design, development and testing of secure embedded and sustainable solutions</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Development" ID="ID_1464737363" CREATED="1621328282468" MODIFIED="1621424736930" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">undertake analysis, needs analysis, design, development and testing of secure embedded and sustainable solutions</font>
    </p>
  </body>
</html></richcontent>
<node TEXT="Python" ID="ID_645157630" CREATED="1621424736848" MODIFIED="1621424740700"/>
<node TEXT="C" ID="ID_1200204337" CREATED="1621424741285" MODIFIED="1621424743559"/>
<node TEXT="C++" ID="ID_1810171371" CREATED="1621424744547" MODIFIED="1621424747384"/>
<node TEXT="Iterative" ID="ID_565778185" CREATED="1621424754668" MODIFIED="1621424758094"/>
<node TEXT="Agile" ID="ID_13416534" CREATED="1621424758564" MODIFIED="1621424760703"/>
</node>
<node TEXT="Testing" ID="ID_965585218" CREATED="1621328290135" MODIFIED="1621424854422" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">undertake analysis, needs analysis, design, development and testing of secure embedded and sustainable solutions</font>
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Diagnosis" ID="ID_1031761539" CREATED="1621328357702" MODIFIED="1621328413661" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Troubleshooting" ID="ID_414039377" CREATED="1621424788515" MODIFIED="1621424793329"/>
</node>
<node TEXT="Maintenance" ID="ID_825695753" CREATED="1621328366915" MODIFIED="1621328413661" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Code structure" ID="ID_1778446816" CREATED="1621424810266" MODIFIED="1621424815536"/>
<node TEXT="Service" ID="ID_1289209931" CREATED="1621424817756" MODIFIED="1621424828203"/>
</node>
<node TEXT="Financial" ID="ID_1323225480" CREATED="1621328380364" MODIFIED="1621328413661" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Cost" ID="ID_1534266582" CREATED="1621424832899" MODIFIED="1621424836416"/>
<node TEXT="Mass production" ID="ID_767334473" CREATED="1621424865186" MODIFIED="1621424870840"/>
<node TEXT="Development cost" ID="ID_85545258" CREATED="1621424875379" MODIFIED="1621424879863"/>
</node>
<node TEXT="Environmental" ID="ID_733350632" CREATED="1621328384552" MODIFIED="1621328413661" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Power consumption" ID="ID_458800293" CREATED="1621424886044" MODIFIED="1621424891403"/>
<node TEXT="Firmware updatable" ID="ID_1617340364" CREATED="1621424923887" MODIFIED="1621424930533"/>
</node>
<node TEXT="Quality" ID="ID_478013920" CREATED="1621328392573" MODIFIED="1621328413661" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt">
<node TEXT="Robustness" ID="ID_80288641" CREATED="1621424943918" MODIFIED="1621424948195"/>
<node TEXT="Precision" ID="ID_434289729" CREATED="1621424956467" MODIFIED="1621424960299"/>
<node TEXT="Validation" ID="ID_381622271" CREATED="1621425009323" MODIFIED="1621425012700"/>
</node>
</node>
<node TEXT="Content" POSITION="left" ID="ID_463127539" CREATED="1621326884713" MODIFIED="1621412839774" STYLE="fork" SHAPE_VERTICAL_MARGIN="0 pt"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font face="sans-serif">Content </font>
    </p>
    <p>
      <font face="sans-serif">The subject component includes </font>
    </p>
    <p>
      <font face="sans-serif">signal handling</font>
    </p>
    <p>
      <font face="sans-serif">component technology</font>
    </p>
    <p>
      <font face="sans-serif">communications </font>
    </p>
    <p>
      <font face="sans-serif">Internet of Things technologies</font>
    </p>
    <p>
      <font face="sans-serif">protocols</font>
    </p>
    <p>
      <font face="sans-serif">interfacing</font>
    </p>
    <p>
      <font face="sans-serif">selection and use of embedded systems and components in integrated solutions. </font>
    </p>
    <p>
      
    </p>
    <p>
      <font face="sans-serif">The subject area is generally concerned with the </font>
    </p>
    <p>
      <font face="sans-serif">design </font>
    </p>
    <p>
      <font face="sans-serif">development </font>
    </p>
    <p>
      <font face="sans-serif">testing </font>
    </p>
    <p>
      <font face="sans-serif">documentation </font>
    </p>
    <p>
      <font face="sans-serif">communication </font>
    </p>
    <p>
      <font face="sans-serif">of secure, sustainable solutions. </font>
    </p>
    <p>
      
    </p>
    <p>
      <font face="sans-serif">ECTS ratingThe Embedded Systems subject component carries 18 ECTS credits.</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</map>
