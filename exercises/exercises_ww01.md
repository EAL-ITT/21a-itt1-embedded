---
Week: 01
tags:
  - RPi Pico
  - RPi
---

# Exercises for ww01

## Exercise 1 - IoT basic system - firmware and testing

### Information

Overview information

### Exercise instructions

**Firmware**

1. Read about firmware in [this article](https://courses.lumenlearning.com/zeliite115/chapter/reading-firmware/). The hacking and security sections are optional, but may be interesting for you.

2. Make a table with 2 columns, describing the main differences between firmware and software. Upload this to your repository.

3. Set up two-way UART communication between RPi and Pico. Reference can be found in exercises week 46. This is only to ensure that there is communication between them. So nothing fancy.

4. Make sure the buttons work. Code to handle the buttons:

   ```py
     from gpiozero import Button
     from time import sleep

     count = 0

     b1 = Button(6)
     b2 = Button(13)
     b3 = Button(19)
     b4 = Button(27)

     def counter():
         global count
         count = count + 1
         print(count)


     while True:
         b1.when_pressed = counter
         b2.when_pressed = counter
         b3.when_pressed = counter
         b4.when_pressed = counter
         sleep(0.1)
   ```

5. Make sure the potentiometer works. Code to handle the potentiometer:

   ```py
    # micropython quick ref for RP2 https://docs.micropython.org/en/latest/rp2/quickref.html#adc-analog-to-digital-conversion
    # micropython ADC class https://docs.micropython.org/en/latest/library/machine.ADC.html#machine-adc

    from machine import ADC, Pin
    from time import sleep

    adc0 = ADC(Pin(26))     # create ADC object on ADC GPIO 26

    while True:
        potentiometer = adc0.read_u16()         # read value, 0-65535 across voltage range 0.0v - 3.3v
        print(potentiometer)
        sleep(0.1)
   ```

6. Read about encapusulation in [this article](https://pynative.com/python-encapsulation/). Write, in your own words, what the difference between public, protected and private members are.
   We will discuss in class with the lecturer guiding you, how these can be used in a firmware conversion of the potmeter and button programs we used.

**Testing**

1. Before we begin software testing, we must make sure there are no short circuits. Follow the guide on [this link](https://resources.altium.com/p/how-test-short-circuit-pcb), and document how the lanes are working. You can use a table and insert + for functional lanes, and - for lanes that may be shortcircuiting.

2. We are going to watch a Socratica video on the unittest module in Python. [![video](https://img.youtube.com/vi/1Lfv5tUGsn8/0.jpg)](https://youtube.com/watch?v=1Lfv5tUGsn8)

3. Read through the [unittest documentation](https://docs.python.org/3/library/unittest.html). It will also be useful later for lookups.

4. Answer the following questions in your group:

   - What are the naming conventions for test functions in unittest?
   - Write down what the differences between assertEqual(), assertTrue(), and assertRaises() are.

5. Use assertTrue() to test that after 10 of any combination of button presses, the counter function returns 10.

6. Use assertEqual() to test that your buttons are returning the same output.

7. Familiarize yourself with other assert functions in unittest. Ctrl + f (ctrl + cmd + f on Mac) in the documentation. How do you assert if a variable is above or below another variable?

8. Discuss in pairs: How can you use assert to test that your potentiometer works within the correct range?

9. Implement assert to test that the potentiometer works to specifications.

10. Reflection question: Would it make sense to use unittest for the display? Make arguments both for and against.

### Optional extras

PyTest library for additional test report capabilities, and perhaps simpler assert syntax [PyTest](https://docs.pytest.org/en/6.2.x/assert.html).

Article (PDF) on firmware vulnerabilities - [Getting a handle on firmware security](https://uefi.org/sites/default/files/resources/Getting%20a%20Handle%20on%20Firmware%20Security%2011.11.17%20Final.pdf).

### Links
