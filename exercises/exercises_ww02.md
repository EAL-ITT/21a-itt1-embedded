---
Week: 02
tags:
  - RPi Pico
  - RPi
  - Recap
---

# Exercises for ww02

## Exercise 1 - Quiz

### Information

First a short quiz on a variety of topics within Embedded Systems. This is to get you started on self-assessment and writing a list of topics for exam preparation, which we are going to expand upon in the other exercises.

### Exercise instructions

Complete the Embedded quiz on Google Forms [https://forms.gle/JU7GoRqp4NuHL4Xj8](https://forms.gle/JU7GoRqp4NuHL4Xj8)

When the quiz is done, review your answers and note what topics you need to brush up on.

You have 15 minutes.

## Exercise 2 - Recap overview

### Information

This exercise helps you to get an overview of what you need to recap before the exam.

### Exercise instructions

1. Create a .md document to keep track of the course learning goals from weekly plans  
   Suggested document layout:

   ```md
   - Week xx - xxxxxxx
     - Learning goal x:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
     - Learning goal y:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
   - Week yy - yyyyy
     - Learning goal z:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
     - Learning goal p:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
   ```

2. In your document extract learning goals from all weeks, including levels.
3. Take some time to assess yourself for all learning goals, noting which levels you meet and which you are missing.

## Exercise 3 - Recap

### Information

This exercise is the actual recap.

### Exercise instructions

According to your self assesment in exercise 1, read material and do exercises from the weeks that you missed learning goals.
