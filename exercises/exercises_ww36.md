---
Week: 36
tags:
- Soldering
- GPIO
- microPython
---


# Exercises for ww36

## Exercise 1 - Get Started with MicroPython on Raspberry Pi Pico chapter 1

### Information

To learn about the RPi Pico and how to use it we will be using parts of a book from Raspberry Pi foundation called "Get Started with MicroPython on Raspberry Pi Pico".
Each week we will have an exercise to share and consolidate knowledge, it is important that you as a team note this knowledge for each embedded systems lecture. The exercise notes will be invaluable when you at the end of the semester recap knowledge before the exam! 

### Exercise instructions

1. Download the "Get Started with MicroPython on Raspberry Pi Pico" book from this link [https://hackspace.raspberrypi.org/books/micropython-pico/pdf](https://hackspace.raspberrypi.org/books/micropython-pico/pdf)

2. Individually read chapter 1 "Get to know your
Raspberry Pi Pico"

3. In your team create a gsuite google team and a shared note document using google docs. I suggest to name the document "Team XX embedded exercise notes"  
Follow the guide at [https://it.ucl.dk/a/1201421-about-g-suite-for-education-at-ucl](https://it.ucl.dk/a/1201421-about-g-suite-for-education-at-ucl)  
Remember to use your UCL credentials to log in to google.

4. In your team answer the below questions and note both questions and answers in your teams note document.

* What is needed to use the RPi Pico on a breadboard ?
* What is GPIO abbreviation of ?
* In which direction does the 2.54 mm male pin headers need to be soldered? Up or down ?
* How are the RPi Pico being powered ?
* What is the purpose of the 'BOOTSEL' button ?
* How many mounting holes does the Pico have ?
* How many GPIO's does the Pico have ?
* Explain with a step-by-step guide how to put the Pico into boot select mode

## Exercise 2 - Prepare RPi Pico Hardware

### Information

Before you can use your Raspberry Pi Pico on a breadboard you need to solder pin headers to it.

### Exercise instructions

1. Go to e-lab (A1.18)
2. Find suitable male pin headers in the component room
3. Solder the headers on the 2 rows of GPIO's. Follow the guide in the book chapter 1 - "Soldering the headers". **Do not solder the debug pins at the end of the Pico!**
4. Clean up according to the guide at the back of the door in  e-lab.

## Exercise 3 - Intall microPython on RPi Pico

### Information

In order to program the RPi Pico using Python you need to install microPython 

### Exercise instructions

1. In your team help each other and follow the guide in the book chapter 1 - "Installing MicroPython".
2. Note your experiences in your teams note document (you might need that knowledge later)

### Links

If needed put links here

