---
Week: 37
tags:
- RPi Pico
- Thonny
---


# Exercises for ww37

## Exercise 1: Gitlab project setup

### Information

This exercise guides you on a gitlab project for hand-ins during the rest of the embedded course.

### Exercise instructions

**Part 1**

You can skip this part if you did it previously or already have a gitlab account

1. Follow the guide at [https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0) to setup GIT and a Gitlab account

**Part 2**

2. Request access to the gitlab group [https://gitlab.com/21a-itt1-embedded-exercises](https://gitlab.com/21a-itt1-embedded-exercises)  
Help on requesting access his here: [https://docs.gitlab.com/ee/user/group/#request-access-to-a-group](https://docs.gitlab.com/ee/user/group/#request-access-to-a-group) 

2. Wait for approval from the group owner (NISI)

3. Setup your gitlab project in the gitlab group [https://gitlab.com/21a-itt1-embedded-exercises](https://gitlab.com/21a-itt1-embedded-exercises)  

    **Naming convention (must be followed strictly!):** 
       
    `<firstname>-<lastname>-programming-exercises`  
    
    **Replace `<firstname>` `<lastname>` with your firstname and lastname, remember the hyphens between words**

## Exercise 2 - Get Started with MicroPython on Raspberry Pi Pico chapter 2

### Information

To learn about the RPi Pico and how to use it we will be using parts of a book from Raspberry Pi foundation called "Get Started with MicroPython on Raspberry Pi Pico".
Each week we will have an exercise to share and consolidate knowledge, it is important that you as a team note this knowledge for each embedded systems lecture. The exercise notes will be invaluable when you at the end of the semester recap knowledge before the exam! 

### Exercise instructions

1. Individually read chapter 2 "Programming
with MicroPython"

2. In your team answer the below questions and note both questions and answers in your teams note document.

* What is Thonny?
* In Thonny, how do you confirm that the Pico is connected ?
* Which operating systems can Thonny be installed on ?
* Where is code executed from when run from Thonny's shell ?
* What does *syntax error* mean ?
* How can you fix a syntax error ?
* How do you save a Python program on the Pico ?
* What is the file extension for Python files?
* Which filenames are not allowed on the Pico ? 
* How do reload a Python program on the Pico ?

## Exercise 3 - Thonny Integrated Development Environment (IDE)
 
### Information

This is an individual exercise but in your team help each other until every team member has completed the exercise.

### Exercise instructions

1. Download Thonny for windows from [https://thonny.org/](https://thonny.org/)
2. Run the installer and follow the on-screen instructions
3. Connect your Pico to Thonny using the instructions in the book, chapter 2, "Connecting Thonny to Pico"
4. Note your experiences in your teams note document (you might need that knowledge later)

## Exercise 4 - Hello world

### Information

Your very first program running in the RPi Pico. Thsi program is using the serial port (UART) to write to the serial terminal built in to Thonny.  
Being able to output messages in your programs is useful for giving information to the user of the program as well as using it for debugging purposes when programming.

### Exercise instructions

1. In your team help each other and follow the book chapter 2 instructions in the section named "Your first MicroPython program: Hello, World!"
2. CHALLENGE: NEW MESSAGE
Can you change the message the Python program prints as its output? If you wanted to add more messages, would you use interactive mode or script mode?  
What happens if you remove the brackets or the quotation marks from the program and then try to run it again?
3. Backup your code to your Gitlab embedded project
4. Note your experiences in your teams note document (you might need that knowledge later)
5. You are welcome to continue chapter if you have the time, but it might require a bit more Python experience to understand what is going on.    
If you do not have experience in Python programming it might not make a lot of sense to do the rest of chapter 2 today.    
In the following weeks, when you know more about the topics, we will do the rest of chapter 2.
