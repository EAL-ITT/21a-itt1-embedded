---
Week: 39
tags:
- RPi Pico
- LED
- Button
- GPIO
---


# Exercises for ww39

## Exercise 1 - Get Started with MicroPython on Raspberry Pi Pico chapter 4

### Information

To learn about the RPi Pico and how to use it we will be using parts of a book from Raspberry Pi foundation called "Get Started with MicroPython on Raspberry Pi Pico".
Each week we will have an exercise to share and consolidate knowledge, it is important that you as a team note this knowledge for each embedded systems lecture. The exercise notes will be invaluable when you at the end of the semester recap knowledge before the exam!   

*This is a team exercise*

### Exercise instructions

1. Individually read chapter 4 "Physical
computing with Raspberry Pi Pico"

2. In your team answer the below questions and note both questions and answers in your teams note document.

* What is the name of the micropython module used to extend micropython for physical computing and how do you use it in a program ? 
* What is the syntax for setting up pin 25 as an output ?
* What does `utime.sleep(10)` do ? 
* What does `led_onboard.toggle()` do ? 
* How are rails and rows connected on a breadboard ?
* Which component is always required when connecting an LED to a GPIO ? (hint: it controls the flow of current through the LED)
* Which of the LED legs should be connected to ground, Anode or Cathode ?
* Explain what a *pull-up* resistor does
* Explain what a *pull-down* resistor does
* How do you set up the internal *pull-down* resistor in micropython?
* How do you detect if a button has been pushed in microptyhon (write the correct line of code)

## Exercise 2 - Knowledge sharing (class)

### Information

This exercise shares knowledge between teams in the class

### Exercise instructions

1. Teams take turns presenting their answers from exercise 1. Please share your document on screen when presenting. 

## Exercise 3 - External LED's

### Information

More than often your embedded system will need more than the built in LED. LED's are, in embedded systems, the only way of determing the state of the system from the outside world.  
It might seem simple but think about your different devices, all of them probably contain at least one led to give you information about the device status.  
As a reflection, from now on, start noticing how your devices use LED's to communicate different states.

### Exercise instructions

1. In your team help each other and follow the book chapter 4 instructions in the sections named "Using a breadboard" and "Next steps: an external LED"
2. CHALLENGE: MULTIPLE LEDS
Can you modify the program to light up both the on‑board and
external LEDs at the same time?  
Can you write a program which lights up the on-board LED when the external LED is switched off, and vice versa?  
Can you extend the circuit to include more than one external LED? Remember, you’ll need a current-limiting resistor for every LED you use!
3. Backup your code to your Gitlab embedded project
4. Note your experiences in your teams note document (you might need that knowledge later)

## Exercise 4 - External Buttons

### Information

Embedded systems often need buttons to navigate and update functionality in the system. Actually being able to add buttons to your embedded system is a vital part of being an embedded developer.
From this exercise you will learn how to connect buttons to your embedded system and how to read them from your program.

### Exercise instructions

1. In your team help each other and follow the book chapter 4 instructions in the sections named "Inputs: reading a button"
2. CHALLENGE: MULTIPLE BUTTONS
Can you add multiple buttons to the circuit and use the in a program?
3. Backup your code to your Gitlab embedded project
4. Note your experiences in your teams note document (you might need that knowledge later)

## Exercise 5 - External LED's and Buttons in the same application

### Information

Overview information

### Exercise instructions

1. In your team help each other and follow the book chapter 4 instructions in the sections named "Inputs and outputs: putting it all together"
2. CHALLENGE: BUILDING IT UP
Can you modify your program so it both lights the LED and
prints a status message to the Shell?  
What would you need to change to make the LED stay on when the button isn’t pressed and switch off when it is?  
Can you add more buttons and LEDs to the circuit?
3. Backup your code to your Gitlab embedded project
4. Note your experiences in your teams note document (you might need that knowledge later)

