---
Week: 41
tags:
- RPi Pico
- BME280
- I2C
- Protocols
- Bus
---


# Exercises for ww41


## Exercise 1 - Serial communication knowledge - I2C (team)

### Information

I2C bus is serial communication for two-wire interface to connect low-speed devices like microcontrollers, EEPROMs, A/D and D/A converters, I/O interfaces and other similar peripherals in embedded systems.

This is the first type of serial communication that you are going to learn about in embedded systems, later in the semester you will learn about Serial Peripheral Interface (SPI) and Universal Asynchronous Receiver/Transmitter (UART)

### Exercise instructions

1. Read about I2C at [https://i2c.info/](https://i2c.info/)
2. Read about the I2C bus specificaton at [https://i2c.info/i2c-bus-specification](https://i2c.info/i2c-bus-specification)
3. Take a brief look at the complete I2C bus specification from NXP, to get familiar with the structure of the document [https://www.pololu.com/file/0J435/UM10204.pdf](https://www.pololu.com/file/0J435/UM10204.pdf)
2. In your team answer the below questions and note both questions and answers in your teams note document.

* Which company invented I2C?
* What is the size of an I2C address in bits?
* What are the names of the 2 wires needed for I2C communication?
* Does I2C require pullup resistors?
* What is the logical levels of SCL and SDA in the *normal state*?
* What is the logical levels of SCL and SDA in the *start condition*?
* What is the logical levels of SCL and SDA in the *stop condition*?
* What is the transfer rate in *fast mode*?
* What is the transfer rate in *high-speed mode*?
* What is OLA15?

*Total: 60 minutes*

## Exercise 2 - I2C knowledge sharing (class)

### Information

This exercise shares knowledge between teams using a cooperative learning structure called *Three for tea*.  
Three for tea is a rapid and efficient way to share knowledge, solutions or ideas among teams.

### Exercise instructions

1. Read the cooperative learning structure called *Three for tea* in the document found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf) (5 minutes)
2. Select a team member to stay at the table (the host), the other teammembers distribute at the 3 other teams tables. (5 minutes)
3. The host presents his/her teams answers to the guests, guests note new knowledge during the presentation. (10 minutes)
4. Guests thanks the host and returns to their team (5 minutes)
5. The team uses *circle of knowledge* to share new knowledge and notes it in the teams note document.(5 minutes)

*Total: 30 minutes*

## Exercise 3 - Using the BME280 sensor - part 1

### Information

This exercise is the I2C bus and the I2C micropython library to communicate with the bosch BME280 and determine it's address

### Exercise instructions

1. Connect the BME280 and your Pico according to the below layout.  
If your BME280 does not have the cs pin, ignore that. The essential connections are SDA, SCL, 3.3V and GND.  

![Pico_bme280_1.png](Pico_bme280_1.png)

2. Create a new .py file in Thonny
3. Run the following micropython program on the Pico

```py
# this script assumes the connection of the I2C bus
# on devices is pin0 = SDA, pin1 = scl

from machine import Pin, SoftI2C # from the machine library

i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)

devices = i2c.scan()

if devices:
    for d in devices:
        print(hex(d))


```

4. Note the address for use in the next exercises

## Exercise 3 - Using the BME280 sensor - part 2

### Information

This exercise is using the I2C bus to communicate with the bosch BME280

### Exercise instructions

1. Using Thonny, create a new folder on your Pico named `lib`
3. Create a new .py file in Thonny
4. Copy the entire content of either `bme280_float.py` or `bme280_int.py` from [https://github.com/robert-hh/BME280](https://github.com/robert-hh/BME280) to the new .py file
5. save the .py file as `BME280.py` in the lib folder on the Pico
6. create a new .py file
7. in the .py file, import libraries  

```py
from machine import Pin, SoftI2C # from the machine library
import BME280 as bme280 # the bme280 library 
```

8. create a soft I2C object 

```py
i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)
```

9. create a BME280 object

```py
bme = bme280.BME280(i2c=i2c, address=your_bme280_address_here)
```

10. Read the sensor and print the values

```py
print(bme.values)
```

11. If you want to print individual values you have to destructure the tuple, as you do not know what tuples are yet just try the below to get familiar with how it works

```py
(temperature, pressure, humidity) = bme.values
print(temperature)
print(pressure)
print(humidity)
```

12. **CHALLENGE** Create an infinite loop that reads and prints the values from the BME280 every 5 seconds

13. Backup your code to your Gitlab embedded project
14. Note your experiences in your teams note document (you might need that knowledge later)

### Links

* BME280 micropython library reference [https://github.com/robert-hh/BME280](https://github.com/robert-hh/BME280)
* BME280 datasheet [https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme280-ds002.pdf](https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme280-ds002.pdf)

## Exercise 4 - Using the BME280 part 3

### Information

Now that you know how to read data from the BME280 you can use it to trigger when certain conditions arise, ie. when one of the readings goes above or below a specific value  

### Exercise instructions

1. Implement the below flow chart in a python file

![BME280 temperature decision](ww41_BME280_temp_condition.png)

3. Backup your code to your Gitlab embedded project
4. Note your experiences in your teams note document (you might need that knowledge later)
5. **CHALLENGE** Can you implement one or more of the other readings (humidity/pressure) into the program?


## Exercise 5 - OLA15

### Information

OLA15 is an Obligatory Learning Activity. Each subject on 1st semester has one or more obligatory learning activities spread throughout the semester.  
Passing obligatory learning activities is a requirement to be admitted to the exam at the end of the semester.
All OLA's are handled in UCL's exam system called *Wiseflow*
You will be notified about new flows in wiseflow through your UCL student email.

### Exercise instructions

1. Find the email from wiseflow in your student email
2. Go to wiseflow, find the assignment for OLA15 and read it to make sure what you have to do and what you are expected to hand-in
3. Find the hand-in time and date. Make sure you hand-in before the deadline

### Links

Read more about obligatory learning activities in the [1st semester description 2021](https://esdhweb.ucl.dk/D21-1625731.pdf?nocache=6acb3332-8857-46bf-974e-21d3f5988674&_gl=1*p7y0er*_up*MQ..*_ga*MTc3MDgwNjM5OS4xNjMzODc5MjY0*_ga_1R6WX2EYMV*MTYzMzg3OTI2My4xLjEuMTYzMzg3OTI2NC4w&_ga=2.88339096.2097698350.1633879264-1770806399.1633879264)

