---
Week: 43
tags:
- RPi
- SSH user agent
- nano
- cheatsheet
---


# Exercises for ww43

## Exercise 1 - RPi SSH command line access

### Information

`SSH` or `Secure shell` is the standard go to protocol to get access to a command prompt on most unix based devices. It is encrypted end-to-end, ie. using proper ciphers and algorithms, eavesdroppers cannot see the information flowing in the session.

There also `scp` which is `secure copy`, and on most installations, if `SSH` is enabled, you may use `scp` to copy files to and from your device in an encrypted fashion. Don't use `ftp` ever - it is obsolete, and has been for a long time.

### Exercise instructions

1. `SSH` is not enabled by default on raspberry pi. Go to the [official documentation](https://www.raspberrypi.org/documentation/remote-access/ssh/) to enable it.

The commands described below is available on most linux devices and when using `git-bash` on windows.

2. To access a devices using the `SSH` protocol, we use the program called `ssh`. See the [official docs](https://www.ssh.com/ssh/) for details.

For most purposes `ssh <user>@<host>` is used. In this example `<user>` is the username on the **remote** device, and `<host>` is the ip address or the host name.

You may use your ssh key to connect to a remote device. The alternative is to use passwords. This is generally considered less secure, and many ssh servers disallows this.

3. To allow login using your ssh key, the file [`.ssh/authorized_keys`](https://www.ssh.com/ssh/authorized_keys/) must be updated to include the **public** part of your ssh keys. Use `ssh-copy-id` for this. See [here](https://www.ssh.com/ssh/copy-id/) for details. The command is `ssh-copy-id <user>@<host>` to enable your default ssh identity.

### Links

If needed put links here

## Exercise 2 - Forwarding SSH key with SSH agent

### Information

SSH keys comes in pairs with a public and a private key.  
Your private SSH key should only be placed on your computer and never exposed to the public, neither placed on multiple machines.  

In order to access remote machines your public key needs to be added to them, but what if you need to access a 2nd remote machine (or gitlab) from the 1st machine you are logged in to?   
This will not work because your private SSH key is not on the first remote machine and it is not supposed to be there.  

The solution is to setup an SSH agent on your local machine, add your private key to the agent and forward that agent when you log on to a remote machine with SSH.

### Exercise instructions

1. Open git bash
2. Enable the ssh agent `eval $(ssh-agent -s)`
3. Add your key to the agent `ssh-add ~/.ssh/id_ed25519`
4. Log on to your Raspberry Pi `ssh -A user@myhost.com` replace user with your RPi username and myhost.com with the ip address of your RPi. The -A parameter is the one that tells SSH to forward your agent
5. When logged on to your RPi test that you can access gitlab with `ssh -T git@gitlab.com`

Using an agent has the added benefit that you only have to type your SSH password one time when starting the agent, the agent will then store your password until it is terminated on reboot.  
After a reboot you need to enable the agent again with `eval $(ssh-agent -s)` 

There is a way to autostart your SSH agent when you launch git bash, for more info see: <a href="https://docs.github.com/en/github/authenticating-to-github/working-with-ssh-key-passphrases#auto-launching-ssh-agent-on-git-for-windows" target="_blank">https://docs.github.com/en/github/authenticating-to-github/working-with-ssh-key-passphrases#auto-launching-ssh-agent-on-git-for-windows</a>

## Exercise 3 - Linux CLI navigation

### Information

Interacting with the RPi through the command line interface requires that you know some basic commands. 
You need to be able to navigate folders, create files and folders etc.

### Exercise instructions

1. Log on to your RPi using SSH and forward your agent with the `-A` parameter
2. Read chapter 1, 2, 3 and 4 in *Essentials - Conquer the Command Line* [https://magpi.raspberrypi.com/books/command-line-second-edition](https://magpi.raspberrypi.com/books/command-line-second-edition) and try out the different commands while reading
3. navigate to your docuents folder using `cd ~/Documents/`
4. On the RPi, Clone your embedded systems project to the RPi documents folder.
5. nvigate to the project foler and create a document called `linux_cmd_cheatsheet.md`  
6. Still on the Pi, edit the document using the `nano` text editor. The contents should be the commands you find most useful, from the book chapters, followed by an explanation that you understand.  
The purpose of the document is to have a quick reference that you can use until you are familiar with the commands :-)
7. `add`, `commit` and `push` the new document to your to the remote repository (gitlab.com)

### Links

Other cheatsheets for inspiration:  
* [https://linuxconfig.org/linux-commands-cheat-sheet](https://linuxconfig.org/linux-commands-cheat-sheet)
* [https://images.linoxide.com/linux-cheat-sheet.pdf](https://images.linoxide.com/linux-cheat-sheet.pdf)

## Exercise 4 - Running a Python program on the RPi

### Information

To make sure that you can run Python programs from the terminal through SSH complete this exercise.

### Exercise instructions

1. check your python3 version using `python3 --version`. If something like **Python 3.7.3** is returned you are good to go. If you do not have python you have to install it using: `sudo apt update` followed by `sudo apt install python3 idle3`
2. navigate to your docuents folder using `cd ~/Documents/`
3. create a new python file with `touch test.py`
4. open the file in nano `nano test.py`
5. insert a print statement `print("Yes I can run Python3 programs on my RPi")`
6. exit nano with `ctrl+x`, enter `y` when prompted and save the file using the same name (just press enter at the part with the filename)
7. execute the program `python3 test.py`
8. if everything is fine you should see your print statement in the terminal

### Links

If needed put links here
