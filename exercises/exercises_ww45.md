---
Week: 45
tags:
- RPi GPIO
- RPi I2C
---


# Exercises for ww45

## Exercise 1 - RPi GPIO - outputs 1

### Information

From previous exercises you know how to use the GPIO pins on the RPi Pico using micropython.  
The RPi has GPIO pins as well but instead of micropython you run regular python. 
You can manipulate the RPi gpio pins with a variety of libraries, the easiest method is using the gpiozero library.

This exercise is about using a GPIO as an output to blink a led.

### Exercise instructions

Everything should be carried out on your RPi

1. Power off your RPi and connect an led with a resistor in series to a GPIO pin.  
Calculate the resistor for a maximum current of app. 10 mA.  
A GPIO pinout is in the official documentation: [https://www.raspberrypi.com/documentation/computers/os.html#gpio-and-the-40-pin-header](https://www.raspberrypi.com/documentation/computers/os.html#gpio-and-the-40-pin-header) and the gpiozero documentation has connection digram [https://gpiozero.readthedocs.io/en/stable/recipes.html#led](https://gpiozero.readthedocs.io/en/stable/recipes.html#led)
2. SSH to your pi, remember to forward the SSH user agent in order to access gitlab.  
See [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww43](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww43) exercise 1 and 2 for details
2. Clone your gitlab embedded project to the RPi into the `/home/Documents folder`
3. Navigate to the cloned folder
4. Create a new virtual environment using `python 3 -m venv env`, install python3-venv if necessary `sudo apt-get install python3-venv`  
For more information on virtual environments see: [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww45](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww45) exercise 2
5. Activate the virtual environment `source env/bin/activate` 
6. Create a new folder called `led_button_test` and navigate to the folder
7. Create a new file called `blink_led.py`
8. Install the GPIO zero python3 package using instructions from [https://gpiozero.readthedocs.io/en/stable/installing.html](https://gpiozero.readthedocs.io/en/stable/installing.html) 
9. Open `blink_led.py` in nano, ie. `sudo nano blink_led.py`
10. Type the below program in `blink_led.py`. Replace your_gpio_pin_here with the GPIO number (integer) your LED is attached to.

```py
from gpiozero import LED
from time import sleep

led = LED(your_gpio_pin_here)

while True:
    led.on()
    sleep(1)
    led.off()
    sleep(1)
``` 

11. exit nano using `ctrl+x` and following the on screen instructions to save the changes
12. run `blink_led.py` using `sudo python3 blink_led.py` 
13. Confirm that the led is blinking, troubleshoot, if not.
14. Add, commit and push the program to gitlab.

### Links

* This video from dronebot workshop explains the RPi GPIO bus as well as how to use the gpiozero library [https://www.youtube.com/watch?v=iL_oZGHLHvU&t=941s](https://www.youtube.com/watch?v=iL_oZGHLHvU&t=941s)

* gpiozero uses the pin library called RPI.GPIO under the hood, but wraps some cool features that RPI.GPIO does not have.   
The RPI.GPIO documentation can be found at [https://sourceforge.net/p/raspberry-gpio-python/wiki/Examples/](https://sourceforge.net/p/raspberry-gpio-python/wiki/Examples/)  

* The gpiozero documentation has a section explaining how it uses RPI.GPIO [https://gpiozero.readthedocs.io/en/stable/migrating_from_rpigpio.html](https://gpiozero.readthedocs.io/en/stable/migrating_from_rpigpio.html)

## Exercise 2 - RPi GPIO - outputs 2

### Information

Instead of just blinking an led you can use Pulse Width Modulation (PWM) to fade the led in and out.  
Don't worry about what pwm is right now, gpiozero makes it very easy to use. 

Everything should be carried out on your RPi.  
This exercise requires that exercise 1 has been completed.

### Exercise instructions

**Program 1 - LED brightness**

1. SSH to your pi, remember to forward the SSH user agent in order to access gitlab. See [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww43](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww43) exercise 1 and 2 for details
2. In the led_button_test folder from exercise 1, create a new file called `led_brightness.py`
3. If not activated, activate the virtual environment `source env/bin/activate` 
7. Open `led_brightness.py` in nano, ie. `sudo nano led_brightness.py`
8. Type and test the first example program at [https://gpiozero.readthedocs.io/en/stable/recipes.html#led-with-variable-brightness](https://gpiozero.readthedocs.io/en/stable/recipes.html#led-with-variable-brightness)
12. Add, commit and push your programs to gitlab while working.

**Program 1 - LED pulse**

1. SSH to your pi, remember to forward the SSH user agent in order to access gitlab. 
2. In the led_button_test folder from exercise 1, create a new file called `led_pulse.py`
3. If not activated, activate the virtual environment `source env/bin/activate` 
7. Open `led_pulse.py` in nano
8. Type and test the second example program at [https://gpiozero.readthedocs.io/en/stable/recipes.html#led-with-variable-brightness](https://gpiozero.readthedocs.io/en/stable/recipes.html#led-with-variable-brightness)
12. Add, commit and push your programs to gitlab while working.


### Links

If you are curious about the what and how of PWM, you can read this article:  
[https://circuitdigest.com/tutorial/what-is-pwm-pulse-width-modulation](https://circuitdigest.com/tutorial/what-is-pwm-pulse-width-modulation)

## Exercise 3 - RPi GPIO - Inputs

### Information

From exercises 1 and 2 you know how to use the GPIO's of the RPi as ouputs.
Of course, you can also use them as inputs.

In this exercise you will create 4 seperate Python programs, that uses a button as GPIO input.

### Exercise instructions

Everything should be carried out on your RPi.  
This exercise requires that exercise 1 has been completed.

1. Power off your RPi and connect a button between a GPIO pin and GND.  
A GPIO pinout is in the official documentation: [https://www.raspberrypi.com/documentation/computers/os.html#gpio-and-the-40-pin-header]
2. SSH to your pi, remember to forward the SSH user agent in order to access gitlab.
8. Type and test the 4 button examples at [https://gpiozero.readthedocs.io/en/stable/recipes.html#button](https://gpiozero.readthedocs.io/en/stable/recipes.html#button) to get familiar with the library button functionality.  
Put each example in it's own mnemonic named python file
12. Add, commit and push your programs to gitlab while working.

*Hint* If you have trouble exiting the python script of registering button presses, try to put a small delay of 0.2 seconds right after reading the button.

## Exercise 4 - RPi I2C 1

### Information

In yoour freenove kit you have a LCD display. The LCD display communicates using I2C. You are already familiar with I2C from the RPi Pico exercises.

This exercise instructs you on how to enable I2C on the RPi, how to search connected devices I2C address and how to get the LCD display up and running.
This exercise is a bit advanced and not as straight forward as the previous exercises (ie. you have to think a bit more).

The exercise uses the Using the freenove tutorial chapter 20 - LCD1602  
[https://github.com/Freenove/Freenove_Ultimate_Starter_Kit_for_Raspberry_Pi/blob/master/Tutorial.pdf](https://github.com/Freenove/Freenove_Ultimate_Starter_Kit_for_Raspberry_Pi/blob/master/Tutorial.pdf)   
and borrows functionality from  
[https://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/python-code](https://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/python-code)

### Exercise instructions

**Show time and CPU temperature**

1. Follow the freenove tutorial chapter 7 p. 111 -112 to configure I2C and Install Smbus on the Rpi. 
2. Fork [https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove](https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove) to your gitlab namespace ie. `https://gitlab.com/<your_gitlab_username>`.  
Forking is a git term that means you create an exact copy of a GIT repository into your own namespace.  
Read more about how forking is done in gitlab here [https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
3. Clone the forked project to your raspberry pi
4. Run `sudo python3 I2CLCD1602.py`
5. If you cannot see anything on the display or the display is not
clear, try adjusting the trim potentiometer on back of LCD1602 slowly, which adjusts the contrast, until the screen can display text clearly.

**show ip and time**

1. Examine the code at [https://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/python-code](https://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/python-code)
2. Make a new python file and combine the code from `I2CLCD1602.py` with the necessary function and variables from the adafruit code, to display your RPi IP address on line 2 of the display while still showing the time and cpu temperature on line 1. 
3. Test and debug while you work.
4. Add, commit and push the changes to gitlab when you are satisfied with the result.

### Links

If you get stuck, a solution can be found here [https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove/-/blob/wip_show_IP/I2CLCD1602_show_ip.py](https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove/-/blob/wip_show_IP/I2CLCD1602_show_ip.py)

## Exercise 5 - RPi I2C 2

### Information

This exercise builds on exercise 4. 

### Exercise instructions

1. Modify the code from exercise 4 to display a welcome message on the display before showing the time, cpu temperature and IP.  
You get to chose the wording of the welcome message :-)
2. Test and debug while you work.
3. Add, commit and push the changes to gitlab when you are satisfied with the result.


### Links

If you get stuck, a solution can be found here [https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove/-/blob/wip_show_IP/I2CLCD1602_show_ip.py](https://gitlab.com/npes-py-experiments/rpi4_lcd1602_freenove/-/blob/wip_show_IP/I2CLCD1602_show_ip.py)

