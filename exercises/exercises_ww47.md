---
Week: 47
tags:
- ADC
- Sampling
- UART
- I2C
---


# Exercises for ww47

## Exercise 1 - ADC Knowledge

### Information

Analog to digital conversion is used whenever you want to read an analog signal from transducers [https://en.wikipedia.org/wiki/Transducer](https://en.wikipedia.org/wiki/Transducer)  

Data aquisition systems including IoT systems often measure analog signals from the outside world to process them digitally.

THe opposite of an ADC exists and is called a digital to analog converter (DAC) and is used as an example when you want to output sound to a speaker.

This exercise gives you a glimpse int the world of analog to digital conversion as well as how the RP2040 and micropython is using ADC's

### Exercise instructions

1. Read about how analog to digital converters work [https://microcontrollerslab.com/analog-to-digital-adc-converter-working/](https://microcontrollerslab.com/analog-to-digital-adc-converter-working/)
2. Read chapter 4.9 in the RP2040 datasheet [https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)
3. Read chapter 3.3 in the Raspberry Pi Pico Python SDK documentation [https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf)

### Links

ADC's have many more characteristics that you would consider if building a professional embedded system for production. Wikioedia has a quite good section about it [https://en.wikipedia.org/wiki/Analog-to-digital_converter](https://en.wikipedia.org/wiki/Analog-to-digital_converter)

## Exercise 2 - ADC knowledge sharing

### Information

In your team agree on answers to the below questions and note them in your team's shared document.  

This exercise is concluded with a conversation on class about your answers.

### Exercise instructions

1. Where can you find specs for the ADC in the RP2040 ?
2. Where can you find Python programming reference for the PICO micropython port ?
3. What is an analog signal ?
4. What is a digital signal ?
5. What is bit depth ?
6. What is sample rate ? 
7. What type of ADC does the RP2040 use ?
8. What is the bit depth of the RP2040 ADC ?
9. What bit depth is used in micropython ?
10. How many ADC channels does the RP2040 have ?
11. How do you instantiate an ADC channel in micropython
12. How do you read the RP2040 internal temperature sensor in micropython ?


## Exercise 3 - RPi Pico ADC and potentiometer

### Information

In this exercise you will create your first embedded system using an ADC. It will read a voltage between 0V and 3.3V controlled by the position of a potentiometer. The exact value of the potentiometer is not important as it is solely used as an adjustable voltage divider [https://www.apogeeweb.net/electron/potentiometer-as-voltage-divider-overview.html](https://www.apogeeweb.net/electron/potentiometer-as-voltage-divider-overview.html)

### Exercise instructions

1. Connect a potentiometer to, 3.3V, GND and GPIO26 on the PICO as shown in the picture  

    ![pico_ADC_potmeter.png](pico_ADC_potmeter.png)

2. On the PICO, create a file named `adc_test_1_input.py`
3. Copy the below program to the file and save it

    ```py
    # micropython quick ref for RP2 https://docs.micropython.org/en/latest/rp2/quickref.html#adc-analog-to-digital-conversion
    # micropython ADC class https://docs.micropython.org/en/latest/library/machine.ADC.html#machine-adc

    from machine import ADC, Pin
    from time import sleep

    adc0 = ADC(Pin(26))     # create ADC object on ADC GPIO 26

    while True:
        potentiometer = adc0.read_u16()         # read value, 0-65535 across voltage range 0.0v - 3.3v
        print(ldr)
        sleep(0.1)
    ```
4. Run the program and turn the potentiometer.  

* If everything is working the shell in thonny should show continious readouts between ~0 and ~65535
* If not, read the instructions again and troubleshoot

## Exercise 4 - RPi pico potentiometer and LDR

### Information

The RP2040 has 5 channels, 3 of them is accessible via the GPIO pins.  
This exercise is using the light dependent resistor (LDR) from the freenove kit on the ADC1 channel.

### Exercise instructions

1. Expand the circuit from exercise 3 with a resistor and an LDR as shown in the image below  

    ![pico_ADC_LDR_potmeter.png](pico_ADC_LDR_potmeter.png)

2. On the PICO, create a file named `adc_test_2_inputs.py`
3. Copy the below program to the file and save it  

    ```py
    # micropython quick ref for RP2 https://docs.micropython.org/en/latest/rp2/quickref.html#adc-analog-to-digital-conversion
    # micropython ADC class https://docs.micropython.org/en/latest/library/machine.ADC.html#machine-adc

    from machine import ADC, Pin
    from time import sleep

    adc0 = ADC(Pin(26))     # create ADC object on ADC GPIO 26
    adc1 = ADC(Pin(27))     # create ADC object on ADC GPIO 27

    while True:
        potentiometer = adc0.read_u16()         # read value, 0-65535 across voltage range 0.0v - 3.3v
        ldr = adc1.read_u16()                   # read value, 0-65535 across voltage range 0.0v - 3.3v    
        print(potentiometer, ldr)
        sleep(0.1)
    ```
4. Open the plotter in Thonny via `view > Plotter`
5. Run the program   
6. Test that the LDR is changing its value by blocking the light from shining on it 

* If everything is working the shell in thonny should show continious readouts between ~0 and ~65535 for both the LDR and the potentiometer
* The plotter should show 2 graphs, one for the potentiometer and one for the LDR
* If not, read the instructions again and troubleshoot

### Links

Read about LDR here [https://www.electroschematics.com/ldr-light-dependent-resistor-photoresistor/](https://www.electroschematics.com/ldr-light-dependent-resistor-photoresistor/)

## Exercise 5 - ADC values over UART to the RPi

### Information

The RPi does not have an ADC and an external ADC is needed to be able to read analog signals on the RPi. Lucky for us the Pico has an ADC and the values can be sent over UART to the RPi.

Expand the UART system from week 46 to include reading of analog signals from the ADC on the Pico.

You can use the LDR and/or potentiometer as in exercise 3 and 4 but you should also try to incorporate reading of the RP2040 internal temperaure sensor as well as the VSYS (board voltage) pin

### Exercise instructions

1. Build the circuit from week 46 exercise 4 or 7
2. Expand the circuit with the circuit from exercise 4
3. Create a flowchart of the expanded system code (expand the flow chart from week 46 exercise 7, draw.io link here [https://gitlab.com/EAL-ITT/21a-itt1-embedded/-/blob/master/docs/flowcharts/ww46_pico_rpi_serialtest_buttons.py.drawio](https://gitlab.com/EAL-ITT/21a-itt1-embedded/-/blob/master/docs/flowcharts/ww46_pico_rpi_serialtest_buttons.py.drawio))
3. Modify your pico UART application from week 46 to handle reading of ADC channels on appropiate command reception (expand the if/else with ie. `potentiometer`, `ldr` and/or `vsys`, `cpu_temp` matching)
4. Modify your RPi UART application from week 46, to include sending commands to read the ADC channels from the pico and show the on the 1602 display.
5. Test and backup your program while you work
