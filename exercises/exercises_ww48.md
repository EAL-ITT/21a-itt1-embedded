---
Week: 48
tags:
- MQTT
- Thingspeak
- Publish/Subscribe
---


# Exercises for ww48


## Exercise 1 - MQTT knowledge

### Information

MQTT is a lightweight publish/subscribe messaging protocol designed for M2M (machine to machine) telemetry in low bandwidth environments.  

MQTT is an abbreviation of MQ Telemetry Transport.  

MQTT is one of the most used protoclos in IoT applications.  

Other IoT protocols are CoAP [http://coap.technology/](http://coap.technology/) and RabbitMQ [https://www.rabbitmq.com/](https://www.rabbitmq.com/)  

MQTT is fairly easy to work with and is supported at both Thingspeak [https://thingspeak.com/](https://thingspeak.com/) and at node-red [https://nodered.org/](https://nodered.org/)  

To get a basic understanding, this exercise takes you on a tour of ressources about MQTT.  
Don't worry if you do not understand everything, later exercises will give you hand on experience to broaden your understanding of how to use it in Python on your raspberry pi.

### Exercise instructions

1. Read about the publish/subscribe pattern [https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern)
2. Read about MQTT [https://en.wikipedia.org/wiki/MQTT](https://en.wikipedia.org/wiki/MQTT)
3. Read more about MQTT [http://www.steves-internet-guide.com/mqtt/](http://www.steves-internet-guide.com/mqtt/)
4. Dissecting MQTT using Wireshark [https://www.catchpoint.com/blog/wireshark-mqtt](https://www.catchpoint.com/blog/wireshark-mqtt)
5. Read about how to use the Paho MQTT Python Client [http://www.steves-internet-guide.com/into-mqtt-python-client/](http://www.steves-internet-guide.com/into-mqtt-python-client/)
6. Read about how to set up MQTT devices on Thingspeak [https://se.mathworks.com/help/thingspeak/mqtt-basics.html](https://se.mathworks.com/help/thingspeak/mqtt-basics.html)
7. Have a brief look at the Paho MQTT Python client library reference [https://pypi.org/project/paho-mqtt/](https://pypi.org/project/paho-mqtt/)


## Exercise 2 - MQTT knowledge sharing

### Information

In your team agree on answers to the below questions and note them in your team's shared document.  

This exercise is concluded with a conversation on class about your answers.

### Exercise instructions

1. What is the purpose of MQTT ?
2. What design pattern is MQTT based on ?
3. Is MQTT a Protocol ?
4. Can MQTT be monitored using wireshark ?
5. What is the name of a popular MQTT library ?
6. How can you install the popular MQTT library ?
7. What is a Thingspeak device ?
8. What is a MQTT broker ?
8. Briefly explain the publish/subsrcibe pattern.


## Exercise 3 - Thingspeak hello world

### Information

This exercise instructs you in setting up a publisher device and a subscriber device that are both connected to the Thingspeak MQTT broker.  
The examples use the paho-mqtt Python client library.  

Data from the publisher device is displayed at Thingspeak and the subscriber device will receive MQTT messages when they are published at the broker.  

The code examples are using a concept called callback functions.  In short a callback function is a regular function that is called from another function. Callback functions run when a certain events occours, like after reading a long file or upon answer from a server.  

In the paho-mqtt library a few different events exists. The examples use the on-connect event (`client.on_connect`) to trigger a function when the client is connected to the broker.  

The subscribe example use the on_message event (`client.on_message`) to trigger a function when a message has been received from the broker.  
Both the on_connect and on_message events contain data sent from the broker.  

The on_connect event passes *client, userdata, flags, rc* to the callback function as parameters and the on_message event passes *client, userdata, message* to the callback functions.  
The passed data are byte encoded json objects that can be handled further in the callback function.  

All paho-mqtt event callbacks are described in the paho-mqtt documentation callback section [https://pypi.org/project/paho-mqtt/#callbacks](https://pypi.org/project/paho-mqtt/#callbacks)  

Callback functions are hard to grasp at first but luckily not very difficult to implement in a program.

### Exercise instructions

**Part 1: Thingspeak setup**

1. Use your matlab credentials to login to [https://thingspeak.com/login?skipSSOCheck=true](https://thingspeak.com/login?skipSSOCheck=true) or create a new account 

*Setup thingspeak channel*

1. Click *channels > my channels* from the top menu  
    ![thingspeak my channels](thingspeak_mychannels.png)
2. Click *new channel*  
    ![thingspeak new channel](thingspeak_new_channel.png)
3. Give the channel a `Name` and rename `Field 1` + `Field 2` 
4. Click *Save Channel*
5. Click *Sharing* and choose *Share channel view with everyone*  
    ![thingspeak channel sharing settings](thingspeak_channel_sharing.png)

Your channel is now created, public and a field is configured
 

**Part 2: Publisher setup**

1. Clone [https://gitlab.com/npes-py-experiments/mqtt-thingspeak](https://gitlab.com/npes-py-experiments/mqtt-thingspeak) to your **Raspberry Pi**
2. Open `publish.py` and analyze the program to get familiar with it. 
3. Follow the instructions in `readme.md` to set up a virtual environment etc.
4. Setup a new MQTT device on Thingspeak.  
Follow the instructions in ***Thingspeak add new MQTT device*** below
5. Save the thingspeak device credentials as `thingspeak_device1_credentials.txt`, in the same folder as the cloned directory.
6. Run `publish.py`
7. In your terminal, ensure that the program is running without errors and that data is shown at thingspeak  
    ![publish.py running](publish_py_connected.png)  
    ![Thingspeak field display](thingspeak_field.png)

**Part 3: Subscriber setup**

1. Clone or fork [https://gitlab.com/npes-py-experiments/mqtt-thingspeak](https://gitlab.com/npes-py-experiments/mqtt-thingspeak) to your **computer**
2. Open `subscribe.py` and analyze the program to get familiar with it.
3. Follow the instructions in `readme.md` to set up a virtual environment etc.
4. Setup a new MQTT device on Thingspeak.  
Follow the instructions in ***Thingspeak add new MQTT device*** below
5. Save the thingspeak device credentials as `thingspeak_device2_credentials.txt`, in the same folder as the cloned directory.
6. Run `subscribe.py`
7. Ensure that the program is running without errors and that received data is shown in your terminal  
    ![subscribe.py running](subscribe_py_connected.png)

**Challenge** Can you iterate the received data in the `on_message(client, userdata, message)` function and print it on seperate lines ?  
(HINT! What datatype is the variable `message_decoded` ??)

***Thingspeak add new MQTT device***

1. Go to MQTT devices  
    ![MQTT devices](thingspeak_MQTT_device.png)
2. Add a new device  
    ![add new MQTT device](thingspeak_add_device.png)
3. Give the device a name and select your channel
    ![configure MQTT device](thingspeak_add_device_form.png)
4. Save credentials as a .txt file  
    ![MQTT device credentials](thingspeak_device_creds.png)

**Troubleshooting**

* If you can't run the applications make sure that you named your credentials as described
* If the program never connects to the broker it might be because port 1883 (mqtt) is closed on your network. The code examples has an optin to use websokets on port 80. It should be obvious what changes you need to make to the code if you read it.
* If you do not receieve MQTT message on the subscriber device make sure that you have set up 2 seperate MQTT devices and you are using the correct credentials file.  
* Wireshark can be useful to troubleshoot MQTT or websockets. Maybe you can find your thingspeak password using wireshark ?
* Thingspeak troubleshoooting section [https://se.mathworks.com/help/thingspeak/troubleshoot-MQTT-publish.html](https://se.mathworks.com/help/thingspeak/troubleshoot-MQTT-publish.html)

## Exercise 4 - Implement MQTT 

### Information

In exercise 3 you got a boilerplate for using MQTT to show data at thingspeak.  
Now it is time to implement MQTT in the code from week 47 exercises.

This exercise will require a lot of code analysis and you will probably like to have analysed and experimented with `publish.py` before merging the example into your own code.  

### Exercise instructions

1. Decide which sensors you would like to display data from, on thingspeak (temperature, humidity, pressure, LDR, potmeter)
2. Expand your flowchart from week 47, exercise 5, with Thingspeak and MQTT.
3. Implement the boilerplate `publish.py` into your code from week 47, exercise 5. Remember to make a copy of you exercise 5 code and install the paho-mqtt (`pip install paho-mqtt`) library in your existing virtual environment 
4. Test that your sensor data is displayed at thingspeak
5. Document with screenshots in your gitlab embedded project
