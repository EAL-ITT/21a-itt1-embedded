---
Week: 49
tags:
- Timers
---


# Exercises for ww49


## Exercise 1 - Timed loop without sleep

### Information

When you design an embedded system you need to think about the system's responsiveness. That translates to how fast the system will respond when the user does something, like pushing a button or a new incoming value from a sensor.  

The optimal will be a system that respond's immediately to all incoming events. But since the nature of a program is that expressions are executed in sequence (line by line) this is not entirely possible.   
However, if the system responds faster than the user, the user will experience the system as being very responsive.
Luckily we are using microcontroller's that can execute millions of command pr. second making it seem to the user that the system is responding immediately.

There is one problem though. Thingspeak only allows us to send data in intervals of minimum 15 seconds. This is, at first sight, not a problem since we can introduce delays using the `time` module's `time.sleep()` method. 

Consider the following:  

```python
import time # used to create intervals

while True:
    time.sleep(15)
    # send values to thingspeak
    # read senors, buttons and update led's here
```

This example will ensure that we are sending data to thingspeak in 15 second intervals, so what's the problem ?  

The problem is that the `time.sleep()` method is a *blocking delay* which means that our program can not do anything in the 15 seconds sleep period, meaning that we can only read buttons, sensor's and update led's every 15 second's.  
That is certainly not faster than the user!

There are many exotic (and complex) ways of solving this problem which we will not dive into. Instead, we will use the current system time to keep track of the loop duration. 

Consider the following ```Loop without sleep```:

```python
import time # used to get timestamp

# define functions
def now_ms():
    ms = time.perf_counter()
    # print(ms)
    return ms

# initialize variables
loop_duration = 15 # in milliseconds
loop_start = now_ms()

while True:
    if (now_ms() - loop_start > loop_duration):
        # send values to Thingspeak here
        print('sending to Thingspeak')
        loop_start = now_ms()
        continue # continue to top of while loop
    else:
        # read senors, buttons and update led's here
        time.sleep(1) # simulates some operations that takes 1 second (remove in live system)
        print(f'loop elapsed ms {now_ms() - loop_start}') # print loop time
```

The program introduces 1 function and 2 variables:  

1. `now_ms()` returns the current time in milliseconds
2. `loop_duration` determines duration of the loop
3. `loop_start` used to keep track of loop time

The program's logic is inside a `While True` endless loop.  
Inside the loop are an if/else block that evaluates if the elapsed loop time is larger than `loop_duration` (15000ms)

The code inside the if statement will only execute when the loop ran for more than 15000ms.   This is where you should send values to Thingspeak, ensuring this only happens in ~15 second intervals.  
The `loop_start` variable is also reset to current time, to determine a new loop start.

The rest of the time is spent inside the else statement.  
This is where you should read your sensors, button's and update the led's.  

The response time of the system is almost immediate and the system is sending values to Thingspeak every ~15 seconds.

The example can be expanded with `elif` statements to do stuff in different intervals, just make sure the intervals are below loop_duration.

**Timer Class**

I wrote a small class/library to simplify using timers, it follows the same principles, which simplifies the above example, especially if you need to use more than one timer as shown below:  

```python
# IMPORT LIBS
from timer import Timer
import time
import logging

# INIT TIMER OBJECTS
timer1 = Timer(name='timer1', logger=logging.warning)
timer2 = Timer(name='timer2', logger=logging.warning)

# START TIMERS
timer1.start()
timer2.start()

# SET LOOP DURS
loop_duration1 = 15 # in seconds
loop_duration2 = 5 # in seconds

while True:
    try:
        if (timer1.elapsed() > loop_duration1):
            # send values to Thingspeak here
            print('sending to Thingspeak')
            timer1.reset()

        elif (timer2.elapsed() > loop_duration2):
            # send values to Thingspeak here
            print('doing something every 5 seconds with timer 2')
            timer2.reset()

        else:
            # read senors, buttons and update led's here
            time.sleep(1) # simulates some operations that takes 1 second (remove in live system)

    except KeyboardInterrupt:
        timer1.stop()
        timer2.stop()
        logging.critical('program terminated by user.......')
        exit(0)
```


You can find the timer class `timer.py` and an example of using it `using_timer_class.py` here:  
[https://gitlab.com/npes-py-experiments/busy-wait-loop](https://gitlab.com/npes-py-experiments/busy-wait-loop)

### Exercise instructions

1. Implement a timer loop, using the timer class, in your raspberry pi program from week 48, exercise 4 according to the below flowchart:  
    ![loop code flowchart](ww49_pico_rpi_loop_buttons.png)

2. Test and debug
3. Upload the code to gitlab

### Links

Code examples:  
[https://gitlab.com/npes-py-experiments/busy-wait-loop](https://gitlab.com/npes-py-experiments/busy-wait-loop) 

## Exercise 2 - Pico/micropython timers

### Information

The pico has "real" timers built in. Timers are an essential part of bare metal programming.

This is a small example of using timers on the Pico, should you need it ?

Micropython on the Pico has 2 different timers. `timer.ONE_SHOT` and `timer.PERIODIC`.

ONE_SHOT runs the callback function 1 time, when the specified time has elapsed.  
PERIODIC runs the callback function continously, when the secified time has elapsed. 

Timers has the advantage over time.sleep() that the processor is not blocked and can do other things in between the timers.

Buttons can be checked using timers, setingt the time at something like 50-100 mS will feel very responsive to the user.  
Normally you then have a boolean variable that is set in the callback function (to not stay in the callback for too long) and then handled in an if statement in the main loop.  
This is illustrated in the below flowchart.

![timer-button-check flowchart](ww49_timer_button.png)

### Exercise instructions

1. Read about micropython RP2040 timer implementation here: [https://docs.micropython.org/en/latest/rp2/quickref.html#timers](https://docs.micropython.org/en/latest/rp2/quickref.html#timers)
2. Read about micropython timer class here: [https://docs.micropython.org/en/latest/library/machine.Timer.html#machine.Timer](https://docs.micropython.org/en/latest/library/machine.Timer.html#machine.Timer)
3. Write this program in Thonny and upload it to you Pico
    ```python
    from machine import Timer, RTC
    import utime


    # JUST TO DISPLAY TIME
    # https://docs.micropython.org/en/latest/library/machine.RTC.html#machine-rtc

    rtc = RTC()
    rtc.datetime((2021, 11, 28, 1, 16, 20, 0, 0)) 

    def seconds_elapsed():
        return str(rtc.datetime()[6])


    # CALLBACK FUNCTIONS
    def callback1():
        print('timer 1 done (ONE_SHOT).' + ' secs elapsed ' + seconds_elapsed())
        
    def callback2():
        print('timer 2 hello (PERIODIC).' + ' secs elapsed ' + seconds_elapsed())

    def callback3():
        print('timer 1 hello (PERIODIC).' + ' secs elapsed ' + seconds_elapsed())

    # INIT TIMERS
    timer1 = Timer(period=5000, mode=Timer.ONE_SHOT, callback=lambda t:callback1())
    timer2 = Timer(period=3000, mode=Timer.PERIODIC, callback=lambda t:callback2())

    # REDEFINE TIMER 1 TO PERIODOC
    timer1.init(period=10000, mode=Timer.PERIODIC, callback=lambda t:callback1())
    ```
4. Analyze and expand the code with atleast one more timer and a callback function that blinks when them timer fires the callback.
5. Evaluate if you need to implement timers on the Pico for your embedded system ?
6. Backup your Pico timer example code to gitlab 

