---
Week: 50
tags:
- IoT basic system build
---


# Exercises for ww50


## Exercise 1 - IoT basic system build - research

### Information

Throughout the embedded course on 1st semester you have been exploring the Raspberry Pi Pico, the Raspberry Pi and various techniques to interface sensors and output devices using micropython and python.

You have also learned how to communicate with Thingspeak, using MQTT, to visualize sensor readings.

You have been building the examples and tasks given using the white solderless proto/bread boards which are fine for small experiments, but they are also qiote fragile and would certainly never make it to production, not even a rough prototype.

Building prototypes that are supposed to run for a longer period of time, either during software development, or even in production, you will normally start with at least a stripboard version. 

On the stripboard you solder suitable pin headers for you modules and micro controllers as well as the peripherals that is part of the system (buttons, led's etc.)

The purpose is to have a build that is durable enough to be running for a longer period of time. Another purpose is to make a more permanent version of the system where devices are attached in a permanent soldered configuration.  
This makes it easier to develop software/firmware for the system. 

The goal is to have a stripboard build of a basic IoT system that looks like this:
![IoT basic system with microcontroller](IoT_basic_system_headers.png)  
*The stripboard layout with RPi and Pico attached*

The below exercises is a lot of work and is spread out on this week as well as next week. Deadline is week 51.

It is very important that you read all the exercises before starting and that you do the exercises in the correct order!

### Exercise instructions

1. Read about stripboard [https://electronicsclub.info/stripboard.htm](https://electronicsclub.info/stripboard.htm)
2. Watch this video to get an overview of different circuit prototyping techniques [https://youtu.be/J9Ig1Sxhe8Y](https://youtu.be/J9Ig1Sxhe8Y)
3. Watch this video for more information on stripboard [https://youtu.be/i_1yAJnbR9U?t=126](https://youtu.be/i_1yAJnbR9U?t=126) 
4. Watch this video to learn stripboard soldering techniques [https://youtu.be/bv2nZJtvrT0](https://youtu.be/bv2nZJtvrT0)

## Exercise 2 - IoT basic system build - preparation

### Information

Now that you know how stripboards works and how to solder them, it is time to prepare for your IoT basic system build.

### Exercise instructions

1. Examine the stripboard layout thoroughly:
    1. Make sure that you identify all breaks in the tracks
    2. Identify which components you need for the build and check them against the supplied *stripboard layout*, especially if your components from are the same sizes ?
    3. Plan the order in which you are going to mount your components (1. IC sockets, 2. Resistors, 3. buttons 4. wires etc.)
    ![IoT basic system with headers](IoT_basic_system_headers.png)  
    *The stripboard layout*
2. Gather your components, IC sockets/pin headers + veroboard can be aquired from yor lecturer, the rest is in elab's component room or part of your freenove kit.
3. Prepare the bme280 pin headers by cutting them to size
4. Prepare the IC socket for the Pico, it needs to be seperated at the middle
5. Break the tracks on the stripboard according to the stripboard layout.  Make sure that you check and double check this step, both before and after, making a break with the 3mm drill
6. Examine all cuts using the microscope in elab to make sure that tracks are cut all the way and you do not have any shorts between tracks.
6. Prepare your tools (soldering iron, solder, wirecutter)


## Exercise 2 - IoT basic system build - veroboard build

### Information

With you tools and planning in place, build the stripboard according to the below steps

### Exercise instructions

1. Build the board according to the layout and following your order for component placement. Make sure to make it neat and pretty with straight wires and solid solder connections.
    ![IoT basic system with headers](IoT_basic_system_headers.png)  
    *The stripboard layout*
2. Measure every connection using a multimeter in continious mode (beep mode) to verify a connection and also making sure that you do not have unwanted connections, especially make sure that you 3.3V, 5V and ground connections are not short circuited!  
Every wrong connection in your build can lead to damaged components!
3. Examine your solder joints using the microscope in e-lab 


## Exercise 3 - IoT basic system build - tests

### Information

The final build step is to methodically test every function of the build.

### Exercise instructions

**0. General**

* Before staring your tests, create a document to note what tests you have performed and what th result of the test are.

**1. RPi tests:**

1. Connect the RPi coppler to the Vero board according to the image below
2. Test that the RPi can boot and nothing on it gets very hot or starts to smell or smoke!
3. Test that the led attached to RPi GPIO22 can be turned on and off from software
4. Test that the 1602 LCD works by writing a small test program for it
5. Test that the buttons connected to GPIO 6, 13, 19 and 26 can be used from software

**2. Pico tests**

1. Connect the Pico to the Vero board according to the image below
2. Test that the Pico can boot and nothing on it gets very hot or starts to smell or smoke!
3. Test that the led attached to GPIO17 can be turned on and off from software
4. Test that the potentiometer 0-3.3V range can be read from ADC0 using software
5. Connect the BME280 and test that all values can be read from software


**3. Combined tests**

1. Test that the RPi and Pico can communicate in UART full duplex
2. Test that you can send data from the BME280, over UART, to Thingspeak using MQTT


![IoT basic system with microcontroller](IoT_basic_system.png)  
*The stripboard layout with RPi and Pico atached*

## Links

* Fritzing files
    * stripboard layout with headers:  
    [https://gitlab.com/EAL-ITT/21a-itt1-embedded/-/blob/master/docs/fritzing/IoT_basic_system_headers.fzz](https://gitlab.com/EAL-ITT/21a-itt1-embedded/-/blob/master/docs/fritzing/IoT_basic_system_headers.fzz)
    * stripboard layout with RPi and Pico:  
    [https://gitlab.com/EAL-ITT/21a-itt1-embedded/-/blob/master/docs/fritzing/IoT_basic_system.fzz](https://gitlab.com/EAL-ITT/21a-itt1-embedded/-/blob/master/docs/fritzing/IoT_basic_system.fzz)
