---
Week: 51
tags:
- IoT basic system build
---


# Exercises for ww51

## Exercise 1 - Continue IoT basic system build

### Information

Continue and finish the IoT basic system build from week 50. 

### Exercise instructions

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww50](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww50)

