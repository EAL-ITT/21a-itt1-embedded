---
Week: 02
Content: Exam recap
Material: See links in weekly plan
Initials: AMNI1
---

# Week 02 - tentative

## Goals of the week(s)

Pratical and learning goals for the period is as follows:

### Practical goals

- Complete exercises from the semester you haven’t completed.
- Complete quiz and self-assessment.

### Learning goals

- None

## Schedule

Zoom link for Tuesday: [https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09](https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09)

Zoom link for Wednesday: [https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09](https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09)

Password: 1234

- 8:15 Introduction (K1)
- 8:30 Evaluation  
   Please fill out this survey [https://forms.gle/Qki6BhVG2NLwVctd9](https://forms.gle/Qki6BhVG2NLwVctd9)
- 8:45 Embedded quiz individual (K4)
- 09:00 Recap + Q&A based on your questions (K1/K4)
- 11:30 Lunch break
- 12:15 Catchup on exercises, prepare for exam (with lecturer, K1/K4)
- 13:00 Catchup on exercises, prepare for exam (without lecturer, K2/K3)
- 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww02](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww02)

## Comments

None.
