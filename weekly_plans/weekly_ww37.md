---
Week: 37
Content: RPi Pico and micropython
Material: See links in weekly plan
Initials: NISI
---

# Week 37

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Install Thonny
* Write "Hello world" program

### Learning goals

* Gitlab setup (LGS3, LGS4, LGC3)
    * Level 1: The student can set up a gitab account
    * Level 2: The student can generate ssh key for gitlab ssh access
    * Level 3: The student can create a gitlab project and use it for simple documentation

* Get Started with MicroPython on Raspberry Pi Pico chapter 2 (LGK6, LGS2, LGS4, LGC1)
    * Level 1: The student knows the purpose of an IDE
    * Level 2: The student knows what code error are
    * Level 3: The student knows how to load files to an embedded system

* Thonny Integrated Development Environment (LGK6, LGS1, LGS2, LGS3, LGS4, LGC1)
    * Level 1: The student can download and install an IDE on a windows computer
    * Level 2: The student can connect an embedded system to an IDE
    * Level 3: The student can, in a team setting, document own work

* Hello world (LGK6, LGS3, LGS4)
    * Level 1: The student can execute code on an embedded system from a shell
    * Level 2: The student can write a basic program
    * Level 3: The student can store and execute a program on an embedded system

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww37](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww37)

## Comments
* none
