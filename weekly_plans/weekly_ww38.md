---
Week: 38
Content: RPi Pico Physical computing
Material: See links in weekly plan
Initials: NISI
---

# Week 38

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Use onboard led
* Code a decision-making program 

### Learning goals
* Knowledge sharing (LGK2, LGK7, LGK8, LGS3, LGC3)
    * Level 1: The student has knowledge about GPIO pins and functions as well as how to operate them from a program
    * Level 2: The student can communicate knowledge from level 1 orally and in writing
    * Level 3: The student can assess knowledge from others and correct his/her own knowledge accordingly

* Physical computing + Conditional execution (LGK7, LGK8, LGS1, LGS2, LGS3, LGC1, LGC2)
    * Level 1: The student knows how to use programming techniques in Python, related to embedded systems physical computing
    * Level 2: The student can use an IDE to write programs and download programs to embedded systems
    * Level 3: The student can, in practice, collaborate with others to acquire new skills and competencies in manipulating GPIO's using embedded systems

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww38](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww38)

## Comments
* None
