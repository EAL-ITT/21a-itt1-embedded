---
Week: 39
Content: RPi Pico Interfacing to hardware - inputs/outputs
Material: See links in weekly plan
Initials: NISI
---

# Week 39 

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Use external led's for output from programs
* Use external button's for input to programs

### Learning goals
* input (LGK1, LGK2, LGK7, LGK8, LGS1, LGS3, LGS4, LGC1)
    * Level 1: The student knows the definition of an input
    * Level 2: The student can use GPIO as an input
    * Level 3: The student can explain GPIO input functionality at a basic level (HW and SW)

* output (LGK1, LGK2, LGK7, LGK8, LGS1, LGS3, LGS4, LGC1)
    * Level 1: The student knows the definition of an output
    * Level 2: The student can use GPIO as an output
    * Level 3: The student can explain GPIO output functionality at a basic level (HW and SW)

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/62627436603](https://ucldk.zoom.us/j/62627436603) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww39](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww39)

## Comments
* videos
    * Digital Inputs with the Raspberry Pi Pico and MicroPython [https://youtu.be/IsWgnU71OiY](https://youtu.be/IsWgnU71OiY)
    * Getting Started with Raspberry Pi Pico with MicroPython - Blinking LED [https://youtu.be/mV6RmdGeQU8](https://youtu.be/mV6RmdGeQU8)
