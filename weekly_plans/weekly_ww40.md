---
Week: 40
Content: No lectures
Material: See links in weekly plan
Initials: NISI
---

# Week 40

## Goals of the week(s)

* This week is without lectures, please use any free time to catch up on exercises from previous weeks.