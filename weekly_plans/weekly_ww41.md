---
Week: 41
Content: RPi Pico Interfacing to hardware - I2C, BME280
Material: See links in weekly plan
Initials: NISI
---

# Week 41

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Complete all exercises
* Complete OLA15

### Learning goals

* I2C
    * Level 1: The student knows what I2C is and what it's usage is
    * Level 2: The student can follow detailed instructions to implement I2C on a RPi Pico using micropython
    * Level 3: The student can implement I2C from a simple flowchart on a RPi Pico using micropython

* BME280
    * Level 1: The student knows the features of the BME280 sensor
    * Level 2: The student can connect the BME280 to an embedded system and use it in a micropython program 
    * Level 3: The student can use individual readings from the BME280 to trigger decisions in a micropython program

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww41](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww41)

## Comments
* None
