---
Week: 45
Content: RPi physical computing - GPIO and I2C
Material: See links in weekly plan
Initials: NISI
---

# Week 45

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Exercises completed and documented on gitlab

### Learning goals
* RPi GPIO
    * Level 1: The student can install a suitable GPIO library on the RPi
    * Level 2: The student can use RPi GPIO as input and output
    * Level 3: The student can use gpiozero to PWM control a LED

* RPi I2C
    * Level 1: The student can set up I2C on the RPi
    * Level 2: The student can fork, pull and run a I2C example program 
    * Level 3: The student can analyze a python program and expand the functionality using functions from another python program

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww45](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww45)

## Comments
* None
