---
Week: 46
Content: RPi Interfacing to hardware - serial communication UART
Material: See links in weekly plan
Initials: NISI
---

# Week 46

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Exercises completed and documented on gitlab

### Learning goals

* Serial communication - UART
    * Level 1: The student has knowldedge about UART and the required physical connections 
    * Level 2: The student can use UART to communicate between a RPi using Python and a RPi Pico using micropython
    * Level 3: The student can use UART in combination with peripherals such as buttons and a display to trigger UART communication between a RPi using Python and a RPi Pico using micropython

## Schedule

* 8:15 Introduction (K1)
    * Video, Understanding UART - Rohde Schwarz [https://youtu.be/sTHckUyxwp8](https://youtu.be/sTHckUyxwp8)
* 9:00 Exercises with lecturer (K1/K4)
* 11:00 Lunch break
* 11:45 Exercises with lecturer (K4)
* 12:45 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww46](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww46)

## Comments
* This week has a lot of exercises that will give you a lot of routine in working with embedded systems.
