---
Week: 47
Content: Sampling - ADC
Material: See links in weekly plan
Initials: NISI
---

# Week 47

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Exercises completed and documented on gitlab
* ADC implemented

### Learning goals
* ADC
    * Level 1: The student knows the purpose of an ADC 
    * Level 2: The student knows key parameters and different ADC types
    * Level 3: The student can implement multichannel ADC readings in an embedded system using micropython and Python 

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/62627436603](https://ucldk.zoom.us/j/62627436603) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 8:15 Introduction (K1)
    * Video, How do ADCs work? - The Learning Circuit [https://youtu.be/g4BvbAKNQ90](https://youtu.be/g4BvbAKNQ90)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww47](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww47)

## Comments
* None