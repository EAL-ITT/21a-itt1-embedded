---
Week: 50
Content:  IoT basic system build
Material: See links in weekly plan
Initials: NISI
---

# Week 50

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* IoT basic system build completed and tested
* Exercises completed and documented on gitlab

### Learning goals
* IoT basic system build
    * Level 1: The student understands the benefits of using a stripboard with soldered components.
    * Level 2: The student can plan a stripboard build from instructions
    * Level 3: The student can build and electrically test a stripboard 

## Schedule

* 8:15 Introduction (K1)
* 9:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:00 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww50](https://eal-itt.gitlab.io/21a-itt1-embedded/exercises/exercises_ww50)

## Comments
* None
